(*<*)theory ConstPropagation imports Semantics "~~/src/HOL/Library/LaTeXsugar" begin(*>*)

section {* Definition des Typsystems *}
text_raw {* 
\begin{frame}{Definition des Typsystems}
  \begin{center}
  \small
*}
text {*
@{text "expr_typing :: \"env \<Rightarrow> expr \<Rightarrow> ty \<Rightarrow> bool\" (\"_ \<turnstile> _ : _\")"} \\[1em]
@{thm[mode=Axiom] TypeValIntg} \textsc{TypeValIntg} \\[1em]
%@{thm[mode=Axiom] TypeValBool} \textsc{TypeValBool} \\[1em]
@{thm[mode=Rule] TypeVar} \textsc{TypeVar} \\[1em]
@{thm[mode=Rule] TypeBopEq} \textsc{TypeBopEq} \\[1em]
%@{thm[mode=Rule] TypeBopAnd} \textsc{TypeBopAnd}
%@{thm[mode=Rule] TypeBopLess} \textsc{TypeBopLess} \\[1em]
@{thm[mode=Rule] TypeBopAdd} \textsc{TypeBopAdd} \\[1em]
%@{thm[mode=Rule] TypeBopSub} \textsc{TypeBopSub}
*}
text_raw {*
  \end{center}
\end{frame}

\begin{frame}{Typisierbarkeit von Anweisungen}
  \begin{center}
  \small
*}
text {*
@{text "WT :: \"env \<Rightarrow> com \<Rightarrow> bool\" (\"_ \<turnstile> _\")"} \\[1em]
@{thm[mode=Axiom] WTSkip} \textsc{WTSkip} \\[1em]
@{thm[mode=Rule] WTLAss} \textsc{WTLAss} \\[1em]
@{thm[mode=Rule] WTSeq} \textsc{WTSeq} \\[1em]
@{thm[mode=Rule] WTCond} \textsc{WTCond} \\[1em]
@{thm[mode=Rule] WTWhile} \textsc{WTWhile} \\[1em]
*}
text_raw {*
  \end{center}
\end{frame}
*}

(*<*)
section {* Semantics *}

subsection {* Determinism *}

text {* 1. Beweis, dass die vorgeg. Semantik deterministisch ist (sowohl im Endzustand, als auch im Zeitbegriff) *}
theorem eval_deterministic:
  assumes eval1: "\<langle>c,s\<rangle> \<Rightarrow> s\<^isub>a : t\<^isub>a"
  assumes eval2: "\<langle>c,s\<rangle> \<Rightarrow> s\<^isub>b : t\<^isub>b"
  shows "s\<^isub>a = s\<^isub>b \<and> t\<^isub>a = t\<^isub>b"
using assms
proof (induction c s "s\<^isub>a" "t\<^isub>a" arbitrary: "s\<^isub>b" "t\<^isub>b" rule:eval.induct)
  case EvalSeq
  from EvalSeq.prems show ?case using EvalSeq.IH
  by (cases rule:eval.cases, auto)
next
  case EvalCondTrue
  from EvalCondTrue.prems show ?case
  proof (cases rule:eval.cases)
    case EvalCondTrue
    thus ?thesis using EvalCondTrue.IH by auto
  next
    case EvalCondFalse
    thus ?thesis using EvalCondTrue.hyps by auto
  qed
next
  case EvalCondFalse
  from EvalCondFalse.prems EvalCondFalse show ?case
  proof (cases rule:eval.cases)
    case EvalCondFalse
    thus ?thesis using EvalCondFalse.IH by auto
  next
    case EvalCondTrue
    thus ?thesis using EvalCondFalse.hyps by auto
  qed
next
  case EvalWhileTrue
  from EvalWhileTrue.prems show ?case
  proof (cases rule:eval.cases)
    case EvalWhileTrue
    thus ?thesis using EvalWhileTrue.IH by auto
  next
    case EvalWhileFalse
    thus ?thesis using EvalWhileTrue.hyps by auto
  qed
qed (auto elim:eval.cases)

corollary eval_deterministic_state:
  "\<langle>\<pi>,\<sigma>\<rangle> \<Rightarrow> \<sigma>' : \<delta>t' \<Longrightarrow> \<langle>\<pi>,\<sigma>\<rangle> \<Rightarrow> \<sigma>'' : \<delta>t'' \<Longrightarrow> \<sigma>' = \<sigma>''"
by (rule conjunct1, rule eval_deterministic, simp_all)

section {* Constant Folding and Propagation *}

(*>*)
section {* Definition der Konstantenfaltung und -propagation *}

(*<*)text {* Folding constant subexpressions *}

text {* @{term constFold} ignores @{term Val} expressions, replaces @{term Var}
        terms with the respective value if present in the map and recursively
        descends into @{term BinOp} expressions. If both operands can be folded
        to values (relative to the state) and the binary operation on these is
        typable, then the whole expression replaced with its constant
        evaluation. *}(*>*)

text_raw {* \begin{frame}{Definition der Konstantenfaltung} *}
fun constFold :: "state \<Rightarrow> expr \<Rightarrow> expr" ("_\<lparr>_\<rparr>"(*<*)[91,0] 90(*>*))
where
  "\<sigma>\<lparr>e\<^isub>1\<guillemotleft>bop\<guillemotright>e\<^isub>2\<rparr> = (case (\<sigma>\<lparr>e\<^isub>1\<rparr>, \<sigma>\<lparr>e\<^isub>2\<rparr>) of
        (Val v\<^isub>1, Val v\<^isub>2) \<Rightarrow> (case binop bop v\<^isub>1 v\<^isub>2 of
            None   \<Rightarrow> Val v\<^isub>1 \<guillemotleft>bop\<guillemotright> Val v\<^isub>2
          | Some v \<Rightarrow> Val v)
        | _                \<Rightarrow> \<sigma>\<lparr>e\<^isub>1\<rparr> \<guillemotleft>bop\<guillemotright> \<sigma>\<lparr>e\<^isub>2\<rparr>)"
  | "\<sigma>\<lparr>Var x\<rparr> = (case \<sigma> x of None \<Rightarrow> Var x | Some v \<Rightarrow> Val v)"
  | "\<sigma>\<lparr>e\<rparr> = e"

text_raw {* \end{frame} *}

(*<*)
text {* Constant Propagation *}
fun isVal :: "expr \<Rightarrow> bool" where
    "isVal (Val _) = True"
  | "isVal _       = False"

lemma isValE:
  assumes "isVal expr"
  obtains val where "expr = Val val"
using assms
by (cases expr, simp_all)

definition map_intersect :: "('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<rightharpoonup> 'b)" ("_ \<inter>\<^sub>m _" [61,61] 60) where
  "map_intersect m1 m2 = (\<lambda>a. if m1 a = m2 a then m1 a else None)"

text {* @{term constPropagate} is counter-intuitively defined as an inductive
        predicate for no other reason than to have all facts that distinguish
        the cases of the same syntactic elements, provided in `this' by the
        automatically generated lemmas inducts and cases. Supposedly, suitable
        elimination rules could have been formulated just as well.

        For assignments we distinguish cases upon whether the variable is
        always assigned the same value or not. Conditionals can in addition to
        simplifications in the condition expression sometimes reduced to either
        the then or the else branch. When optimizing loop conditions it is
        important to only fold terms and not resolve variables, but clear the
        propagation map alltogether. The only exception occurs when the
        condition always evaluates to @{term false}. After the introduction of
        a new constant variable the propagation state is updated with this
        assignment, whereas after an unreduced conditional only common
        constants among the per branch mappings are valid in the remaining
        program and after a loop any information about constant variables is
        lost. The sequencing of statements pushes those constant variables
        information onwards to the next statement. *}
(*>*)

text_raw {* \begin{frame}[fragile]{Definition der Konstantenpropagation} *}
inductive constPropagate :: "state \<Rightarrow> com \<Rightarrow> state \<Rightarrow> com \<Rightarrow> bool" ("\<langle>_,_\<rangle> \<leadsto> \<langle>_,_\<rangle>"(*<*)[0,0,0,0] 50(*>*))
(*<*)
  where
        Skip:
          "constPropagate \<sigma> Skip \<sigma> Skip"
      | LAssVal:
          "constFold \<sigma> e = Val v
          \<Longrightarrow> constPropagate \<sigma> (x::=e) (\<sigma>(x \<mapsto> v)) (x::=Val v)"
      | LAssNonVal:
          "\<lbrakk>constFold \<sigma> e = e'; \<not>isVal e'\<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (x::=e) (\<sigma>(x:=None)) (x::=e')"
      | Seq:
          "\<lbrakk> constPropagate \<sigma> c\<^isub>1 \<sigma>'' c\<^isub>1' ; constPropagate \<sigma>'' c\<^isub>2 \<sigma>' c\<^isub>2' \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (c\<^isub>1;; c\<^isub>2) \<sigma>' (c\<^isub>1';; c\<^isub>2')"
      | CondTrue:
          "\<lbrakk> constFold \<sigma> b = Val true ; constPropagate \<sigma> c\<^isub>1 \<sigma>' c\<^isub>1' \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (IF (b) c\<^isub>1 ELSE c\<^isub>2) \<sigma>' c\<^isub>1'"
      | CondFalse:
          "\<lbrakk> constFold \<sigma> b = Val false ; constPropagate \<sigma> c\<^isub>2 \<sigma>' c\<^isub>2' \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (IF (b) c\<^isub>1 ELSE c\<^isub>2) \<sigma>' c\<^isub>2'"
      | Cond:
          "\<lbrakk> constFold \<sigma> b = b' ; b' \<noteq> Val false; b' \<noteq> Val true; constPropagate \<sigma> c\<^isub>1 \<sigma>\<^isub>1' c\<^isub>1' ; constPropagate \<sigma> c\<^isub>2 \<sigma>\<^isub>2' c\<^isub>2' \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (IF (b) c\<^isub>1 ELSE c\<^isub>2) (\<sigma>\<^isub>1' \<inter>\<^sub>m \<sigma>\<^isub>2') (IF (b') c\<^isub>1' ELSE c\<^isub>2')"
      | WhileFalse:
          "\<lbrakk> constFold \<sigma> b = Val false \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (WHILE (b) c) \<sigma> Skip"
      | While:
          "\<lbrakk> constFold \<sigma> b \<noteq> Val false; constPropagate empty c \<sigma>'' c' \<rbrakk>
          \<Longrightarrow> constPropagate \<sigma> (WHILE (b) c) empty (WHILE (constFold empty b) c')"
(*>*)
text_raw {* 
  \begin{center}
  \small
*}
text {*
@{thm[mode=Axiom] Skip} \textsc{Skip} \\[1em]
@{thm[mode=Rule] LAssVal} \textsc{LAssVal} \\[1em]
@{thm[mode=Rule] LAssNonVal} \textsc{LAssNonVal} \\[1em]
@{thm[mode=Rule] Seq} \textsc{Seq} \\[1em]
*}
text_raw {*
  \end{center}
\end{frame}

\begin{frame}[fragile]{Definition der Konstantenpropagation}
  \begin{center}
*}
text {*
@{thm[mode=Rule] CondTrue} \textsc{CondTrue} \\[1em]
%@{thm[mode=Rule] CondFalse} \textsc{CondFalse} \\[1em]
@{thm[mode=Rule] Cond} \textsc{Cond} \\[1em]
@{thm[mode=Rule] WhileFalse} \textsc{WhileFalse} \\[1em]
@{thm[mode=Rule,names_short] While} \textsc{While} \\[1em]
*}
text_raw {*
  \end{center}
\end{frame}
*}

section {* Rückführung auf eine Funktion *}

text_raw {* \begin{frame}{Rückführung auf eine Funktion} *}

lemma constPropagate_deterministic:
  "\<lbrakk>\<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>; \<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>'',\<pi>''\<rangle>\<rbrakk> \<Longrightarrow> \<sigma>' = \<sigma>'' \<and> \<pi>' = \<pi>''"
proof (induction (*<*)arbitrary: \<sigma>'' \<pi>'' (*>*)rule:constPropagate.induct)
  txt_raw {* \\ \ldots \\ *}
(*<*)
  case Seq
  from Seq.IH Seq.prems
    show ?case
    by (auto elim: constPropagate.cases)
next
  case CondTrue
  from CondTrue.hyps(1) CondTrue.IH CondTrue.prems
    show ?case
    by (auto elim: constPropagate.cases)
next
  case CondFalse
  from CondFalse.hyps(1) CondFalse.IH CondFalse.prems
    show ?case
    by (auto elim: constPropagate.cases)
next
  case Cond
  from Cond.hyps(1,2,3) Cond.IH Cond.prems
    show ?case
    by (auto elim: constPropagate.cases)
next
  case While
  from While.hyps(1) While.prems While.IH
    show ?case
    by (auto elim: constPropagate.cases)
(*>*)
qed(*<*) (auto elim: constPropagate.cases)(*>*)

text {* \vspace{0.5em} *}

lemma constPropagate_total:
  (*<*)obtains \<sigma>' \<pi>' where "\<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>"(*>*)
  txt_raw {* @{text "\<exists>\<sigma>' \<pi>'. \<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>"} \\ *}
proof (induction \<pi>(*<*) arbitrary: \<sigma>(*>*))
  txt_raw {* \\ \ldots \\ *}
(*<*)
  case Skip
  thus ?case using constPropagate.Skip by auto
next
  case (LAss _ expr)
  thus ?case by (auto intro:LAssVal LAssNonVal isValE)
next
  case Seq
  thus ?case by (metis constPropagate.Seq)
next
  case Cond
  thus ?case by (metis CondFalse constPropagate.Cond CondTrue)
next
  case While
  thus ?case by (metis WhileFalse constPropagate.While)
(*>*)
qed

theorem constPropagate_is_fun:
  "\<exists>!\<sigma>' \<pi>'. \<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>"
using constPropagate_total constPropagate_deterministic(*<*) by smt(*>*)

text_raw {* \end{frame} *}
(*<*)

definition constPropagateFun :: "state \<Rightarrow> com \<Rightarrow> state \<times> com" where
  "constPropagateFun s c = (THE (s',c'). constPropagate s c s' c')"

subsection {* Correctness *}

subsubsection {* Folding *}

lemma map_le:
  assumes "f \<subseteq>\<^sub>m g" "x \<in> dom f"
  obtains "f x = g x"
using assms unfolding map_le_def by auto

lemma map_intersect_le1:
  shows "m \<inter>\<^sub>m m' \<subseteq>\<^sub>m m"
unfolding map_intersect_def map_le_def
by (auto split:bool.split)

lemma map_intersect_le2:
  shows "m \<inter>\<^sub>m m' \<subseteq>\<^sub>m m'"
unfolding map_intersect_def map_le_def
by (auto split:bool.split)

lemma constFold_dist: "\<lbrakk>constFold s (lexpr \<guillemotleft>bop\<guillemotright> rexpr)\<rbrakk>\<sigma> = \<lbrakk>constFold s lexpr \<guillemotleft>bop\<guillemotright> constFold s rexpr\<rbrakk>\<sigma>"
by (auto split:expr.split option.split)

theorem constFold_correct: "\<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma> \<Longrightarrow> \<lbrakk>constFold \<sigma>\<^isub>p expr\<rbrakk>\<sigma> = \<lbrakk>expr\<rbrakk>\<sigma>"
by (induction expr \<sigma> rule:interpret.induct, simp_all only:constFold_dist, auto elim:map_le split:option.split)

lemma constFold_correctE:
  assumes "\<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma>"
  obtains "\<lbrakk>constFold \<sigma>\<^isub>p expr\<rbrakk>\<sigma> = \<lbrakk>expr\<rbrakk>\<sigma>"
using assms by (auto intro:constFold_correct)

lemma constFold_correct_empty:
  shows "\<lbrakk>constFold empty expr\<rbrakk>\<sigma> = \<lbrakk>expr\<rbrakk>\<sigma>"
using assms by (auto intro:constFold_correct)

lemma constFold_eval:
  assumes "constFold \<sigma>\<^isub>p expr = Val val" "\<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma>" 
  obtains "\<lbrakk>expr\<rbrakk>\<sigma> = Some val"
using assms by (auto elim!:constFold_correctE[where expr=expr])

subsubsection {* Propagation *}

lemma SeqE:
  assumes "\<langle>c\<^isub>1;;c\<^isub>2,s\<rangle> \<Rightarrow> s' : t"
  obtains s'' t\<^isub>1 t\<^isub>2 where "\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s'' : t\<^isub>1" "\<langle>c\<^isub>2,s''\<rangle> \<Rightarrow> s' : t\<^isub>2" "t = Suc (t\<^isub>1 + t\<^isub>2)"
using assms by (auto elim:eval.cases)

lemma CondTrueE:
  assumes "\<langle>IF (b) c\<^isub>1 ELSE c\<^isub>2,s\<rangle> \<Rightarrow> s' : t" "\<lbrakk>b\<rbrakk> s = Some true"
  obtains t' where "\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s' : t'" "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')"
using assms by (auto elim:eval.cases)

lemma CondFalseE:
  assumes "\<langle>IF (b) c\<^isub>1 ELSE c\<^isub>2,s\<rangle> \<Rightarrow> s' : t" "\<lbrakk>b\<rbrakk> s = Some false"
  obtains t' where "\<langle>c\<^isub>2,s\<rangle> \<Rightarrow> s' : t'" "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')"
using assms by (auto elim:eval.cases)

lemma CondE:
  assumes "\<langle>IF (b) c\<^isub>1 ELSE c\<^isub>2,s\<rangle> \<Rightarrow> s' : t"
  obtains t' where "(\<lbrakk>b\<rbrakk> s = Some true) \<and> (\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s' : t') \<or> (\<lbrakk>b\<rbrakk> s = Some false) \<and> (\<langle>c\<^isub>2,s\<rangle> \<Rightarrow> s' : t')"  "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')"
using assms by (auto elim:eval.cases)

lemma LassValE:
  assumes "\<langle>x::=e,s\<rangle> \<Rightarrow> s' : t"
  obtains "s' = s(x := \<lbrakk>e\<rbrakk>s)"
using assms by (auto elim:eval.cases)

lemma WhileFalseE:
  assumes "\<langle>WHILE (b) c,s\<rangle> \<Rightarrow> s' : t" "\<lbrakk>b\<rbrakk> s = Some false"
  obtains s'' t' t'' where "(s' = s) \<and> (t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t))"
using assms by (cases rule:eval.cases, auto)

lemma constPropagate_partial_eval:
  "constPropagate \<sigma>\<^isub>p \<pi> \<sigma>\<^isub>c \<pi>\<^isub>c \<Longrightarrow> \<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma> \<Longrightarrow> \<langle>\<pi>, \<sigma>\<rangle> \<Rightarrow> \<sigma>' : \<delta>t' \<Longrightarrow> \<sigma>\<^isub>c \<subseteq>\<^sub>m \<sigma>'"
proof (induction arbitrary:\<delta>t' \<sigma> \<sigma>' rule:constPropagate.induct)
  case Skip
  thus ?case by (auto elim:eval.cases)
next
  case LAssVal
  thus ?case by (metis EvalLAss constFold_eval eval_deterministic_state map_le_upd)
next
  case LAssNonVal
  thus ?case by (metis EvalLAss domD domIff eval_deterministic_state map_le_imp_upd_le map_le_upd)
next
  case Seq
  thus ?case by (smt SeqE)
next
  case CondTrue
  thus ?case by (auto elim:constFold_eval CondTrueE)
next
  case CondFalse
  thus ?case by (auto elim:constFold_eval CondFalseE)
next
  case (Cond \<sigma>\<^isub>p b b' \<pi>\<^isub>t \<sigma>\<^isub>s\<^isub>t \<pi>'\<^isub>t \<pi>\<^isub>e \<sigma>\<^isub>s\<^isub>e \<pi>'\<^isub>e)
  hence "\<sigma>\<^isub>s\<^isub>t \<subseteq>\<^sub>m \<sigma>' \<or> \<sigma>\<^isub>s\<^isub>e \<subseteq>\<^sub>m \<sigma>'" by (auto elim:eval.cases)
  thus ?case using map_intersect_le1[of \<sigma>\<^isub>s\<^isub>t \<sigma>\<^isub>s\<^isub>e] map_intersect_le2[of \<sigma>\<^isub>s\<^isub>t \<sigma>\<^isub>s\<^isub>e] by (auto intro:map_le_trans)
next
  case WhileFalse
  thus ?case by (metis EvalWhileFalse constFold_eval eval_deterministic_state)
next
  case While
  show ?case by (rule map_le_empty)
qed

theorem constPropagate_correct:
  "constPropagate \<sigma>\<^isub>p \<pi> \<sigma>\<^isub>c \<pi>\<^isub>c \<Longrightarrow> \<langle>\<pi>, \<sigma>\<rangle> \<Rightarrow> \<sigma>' : \<delta>t' \<Longrightarrow> \<langle>\<pi>\<^isub>c, \<sigma>\<rangle> \<Rightarrow> \<sigma>'' : \<delta>t'' \<Longrightarrow> \<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma> \<Longrightarrow> \<sigma>' = \<sigma>''"
proof (induction \<sigma>\<^isub>p \<pi> \<sigma>\<^isub>c \<pi>\<^isub>c arbitrary:\<sigma> \<sigma>' \<sigma>'' \<delta>t' \<delta>t'' rule:constPropagate.induct)
  case Skip
  thus ?case by - (rule eval_deterministic_state)
next
  case LAssVal
  thus ?case by (auto elim:constFold_eval elim!:eval.cases)
next
  case LAssNonVal
  thus ?case by (metis LassValE constFold_correctE)
next
  case Seq
  thus ?case by (smt SeqE constPropagate_partial_eval)
next
  case CondTrue
  thus ?case by (metis CondTrueE constFold_eval)
next
  case CondFalse
  thus ?case by (metis CondFalseE constFold_eval)
next
  case Cond
  thus ?case by (auto elim!:CondE iff:constFold_correct)
next
  case WhileFalse
  thus ?case by (metis EvalSkip EvalWhileFalse constFold_eval eval_deterministic_state)
next
  case (While \<sigma>\<^isub>p expr \<pi> \<sigma>'\<^isub>p \<pi>' \<sigma> \<sigma>' \<sigma>'' \<delta>t' \<delta>t'')
  from While.prems(1,2) show ?case
  proof (induction "WHILE (expr) \<pi>" \<sigma> \<sigma>' \<delta>t' arbitrary: \<delta>t'' rule:eval.induct)
    case EvalWhileFalse
    thus ?case using constFold_correct_empty by (auto elim:eval.cases)
  next
    case (EvalWhileTrue \<sigma> \<sigma>\<^isub>i t\<^isub>c \<sigma>' t\<^isub>w)
    hence "\<lbrakk>constFold empty expr\<rbrakk>\<sigma> = Some true" using constFold_correct_empty by auto
    with EvalWhileTrue(6) obtain \<sigma>\<^isub>c \<delta>t\<^isub>c \<delta>t\<^isub>w
      where "\<langle>\<pi>',\<sigma>\<rangle> \<Rightarrow> \<sigma>\<^isub>c : \<delta>t\<^isub>c"
        and "\<langle>WHILE (constFold empty expr) \<pi>',\<sigma>\<^isub>c\<rangle> \<Rightarrow> \<sigma>'' : \<delta>t\<^isub>w"
      by (auto elim:eval.cases)
    moreover
      with While.IH EvalWhileTrue.hyps have "\<sigma>\<^isub>i = \<sigma>\<^isub>c" by simp
    ultimately show ?case using EvalWhileTrue by simp
  qed
qed

subsection {* Run Time *}

theorem constFold_faster: "\<lbrakk>constFold s e\<rbrakk>\<^sub>t \<le> \<lbrakk>e\<rbrakk>\<^sub>t"
by (induct rule:interpretTime.induct) (auto split:option.split expr.split)

theorem constPropagate_faster:
  assumes "constPropagate r c r\<^isub>p c\<^isub>p" "r \<subseteq>\<^sub>m s" "\<langle>c,s\<rangle> \<Rightarrow> s' : t" "\<langle>c\<^isub>p,s\<rangle> \<Rightarrow> s'\<^isub>p : t\<^isub>p"
  shows "t\<^isub>p \<le> t"
using assms
proof (induction arbitrary: s s' t s'\<^isub>p t\<^isub>p rule:constPropagate.induct)
  case Skip
  thus ?case by (auto elim:eval.cases)
next
  case LAssVal
  thus ?case by (smt EvalLAss constFold_faster eval_deterministic)
next
  case LAssNonVal
  thus ?case by (auto elim:eval.cases intro:constFold_faster)
next
  case (Seq r c\<^isub>1 r' c\<^isub>1\<^isub>p c\<^isub>2 r'' c\<^isub>2\<^isub>p s s' t s'\<^isub>p t\<^isub>p)
  from Seq obtain s'' t\<^isub>1 t\<^isub>2
    where 1: "\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s'' : t\<^isub>1"
             "\<langle>c\<^isub>2,s''\<rangle> \<Rightarrow> s' : t\<^isub>2"
             "t = Suc (t\<^isub>1 + t\<^isub>2)"
    by (auto elim:SeqE)
  from Seq obtain s''\<^isub>p t\<^isub>1\<^isub>p t\<^isub>2\<^isub>p
    where 2: "\<langle>c\<^isub>1\<^isub>p,s\<rangle> \<Rightarrow> s''\<^isub>p : t\<^isub>1\<^isub>p"
             "\<langle>c\<^isub>2\<^isub>p,s''\<^isub>p\<rangle> \<Rightarrow> s'\<^isub>p : t\<^isub>2\<^isub>p"
             "t\<^isub>p = Suc (t\<^isub>1\<^isub>p + t\<^isub>2\<^isub>p)"
    by (auto elim:SeqE)
  from Seq 1 2 have 3: "s'' = s''\<^isub>p" by (auto elim!:constPropagate_correct)
  from Seq 1 2 have 4: "r' \<subseteq>\<^sub>m s''" by -(rule constPropagate_partial_eval)
  from Seq 1 2 3 4 have "t\<^isub>1\<^isub>p \<le> t\<^isub>1" "t\<^isub>2\<^isub>p \<le> t\<^isub>2" by auto
  with 1 2 show ?case by auto
next
  case (CondTrue r b c\<^isub>1 r' c\<^isub>1\<^isub>p c\<^isub>2 s s' t s'\<^isub>p t\<^isub>p)
  then obtain t' where "\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s' : t'" and [simp]: "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')" by (auto elim:CondTrueE constFold_eval)
  hence "t\<^isub>p \<le> t'" using CondTrue by auto
  thus ?case by auto
next
  case (CondFalse r b c\<^isub>2 r' c\<^isub>2\<^isub>p c\<^isub>1 s s' t s'\<^isub>p t\<^isub>p)
  then obtain t' where "\<langle>c\<^isub>2,s\<rangle> \<Rightarrow> s' : t'" and [simp]: "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')" by (auto elim:CondFalseE constFold_eval)
  hence "t\<^isub>p \<le> t'" using CondFalse by auto
  thus ?case by auto
next
  case (Cond r b b' c\<^isub>1 r' c\<^isub>1\<^isub>p c\<^isub>2 r'' c\<^isub>2\<^isub>p s s' t s'\<^isub>p t\<^isub>p)
  from Cond obtain t'
    where 1: "(\<lbrakk>b\<rbrakk> s = Some true) \<and> (\<langle>c\<^isub>1,s\<rangle> \<Rightarrow> s' : t') \<or> (\<lbrakk>b\<rbrakk> s = Some false) \<and> (\<langle>c\<^isub>2,s\<rangle> \<Rightarrow> s' : t')"
             "t = Suc (\<lbrakk>b\<rbrakk>\<^sub>t + t')"
    by (auto elim:CondE)
  from Cond obtain t'\<^isub>p
    where 2: "(\<lbrakk>constFold r b\<rbrakk> s = Some true) \<and> (\<langle>c\<^isub>1\<^isub>p,s\<rangle> \<Rightarrow> s'\<^isub>p : t'\<^isub>p) \<or> (\<lbrakk>constFold r b\<rbrakk> s = Some false) \<and> (\<langle>c\<^isub>2\<^isub>p,s\<rangle> \<Rightarrow> s'\<^isub>p : t'\<^isub>p)"
             "t\<^isub>p = Suc (\<lbrakk>constFold r b\<rbrakk>\<^sub>t + t'\<^isub>p)"
    by (auto elim:CondE)
  from Cond 1 2 have 3: "t'\<^isub>p \<le> t'" by -(frule constFold_correctE[where expr=b], auto)
  have "\<lbrakk>constFold r b\<rbrakk>\<^sub>t \<le> \<lbrakk>b\<rbrakk>\<^sub>t" by (rule constFold_faster)
  with 1 2 3 show ?case by auto
next
  case WhileFalse
  thus ?case by (smt EvalSkip EvalWhileFalse constFold_eval eval_deterministic)
next
  case (While r b c r' c' s s' t s'\<^isub>p t\<^isub>p)
  from While.prems(2,3) While.hyps(2)
  show ?case
  proof (induction "WHILE (b) c" s s' t arbitrary: r t\<^isub>p rule:eval.induct)
    case (EvalWhileFalse r t\<^isub>p)
    thus ?case using constFold_correct_empty constFold_faster by (auto elim:WhileFalseE)
  next
    case (EvalWhileTrue s s'' t\<^isub>c s' t\<^isub>w t\<^isub>p)
    from `\<langle>WHILE (constFold empty b) c',s\<rangle> \<Rightarrow> s'\<^isub>p : t\<^isub>p` `\<lbrakk>b\<rbrakk> s = Some true`
    obtain s''\<^isub>p t\<^isub>c\<^isub>p t\<^isub>w\<^isub>p where 1: "\<langle>c',s\<rangle> \<Rightarrow> s''\<^isub>p : t\<^isub>c\<^isub>p" "\<langle>WHILE (constFold empty b) c',s''\<^isub>p\<rangle> \<Rightarrow> s'\<^isub>p : t\<^isub>w\<^isub>p"
      "t\<^isub>p = Suc (\<lbrakk>constFold empty b\<rbrakk>\<^sub>t + t\<^isub>c\<^isub>p + t\<^isub>w\<^isub>p)"
      using constFold_correct_empty by (auto elim:eval.cases)
    with While.IH EvalWhileTrue have 2: "t\<^isub>c\<^isub>p \<le> t\<^isub>c" by auto
    from 1 EvalWhileTrue have [simp]: "s'' = s''\<^isub>p" by -(rule constPropagate_correct[of Map.empty], auto)
    from 1 EvalWhileTrue have "t\<^isub>w\<^isub>p \<le> t\<^isub>w" by auto
    with 1 2 constFold_faster show ?case by smt
  qed
qed

subsection {* Program Size *}

theorem constFold_smaller: "size (constFold r e) \<le> size e"
by (induction rule:constFold.induct, auto split:expr.split option.split)

fun comSize :: "com \<Rightarrow> nat" where
  "comSize Skip = 1"
  | "comSize (x::=e) = Suc (size e)"
  | "comSize (c\<^isub>1;;c\<^isub>2) = Suc (comSize c\<^isub>1 + comSize c\<^isub>2)"
  | "comSize (IF (b) c\<^isub>t ELSE c\<^isub>f) = Suc (size b + comSize c\<^isub>t + comSize c\<^isub>f)"
  | "comSize (WHILE (b) c) = Suc (size b + comSize c)"

theorem constPropagate_smaller: "constPropagate r c r' c' \<Longrightarrow> comSize c' \<le> comSize c"
proof (induction rule:constPropagate.induct)
  case (Cond s b)
  thus ?case using constFold_smaller[of s b] by auto
next
  case (While s b)
  thus ?case using constFold_smaller[of empty b] by auto
qed (auto intro:constFold_smaller)

subsection {* Idempotence *}

subsubsection {* Folding *}

lemma constFoldBinOpE:
  fixes \<sigma> \<alpha>\<^isub>1 \<gamma> \<alpha>\<^isub>2
  obtains (Val) \<omega> \<omega>\<^isub>1 \<omega>\<^isub>2
            where "constFold \<sigma> (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2) = Val \<omega>"
                  "constFold \<sigma> \<alpha>\<^isub>1 = Val \<omega>\<^isub>1"
                  "constFold \<sigma> \<alpha>\<^isub>2 = Val \<omega>\<^isub>2"
                  "binop \<gamma> \<omega>\<^isub>1 \<omega>\<^isub>2 = Some \<omega>"
        | (BinOp) \<alpha>\<^isub>1' \<alpha>\<^isub>2'
            where "constFold \<sigma> (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2) = (\<alpha>\<^isub>1' \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2')"
                  "constFold \<sigma> \<alpha>\<^isub>1 = \<alpha>\<^isub>1'"
                  "constFold \<sigma> \<alpha>\<^isub>2 = \<alpha>\<^isub>2'"
proof (cases "constFold \<sigma> (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)")
  case (BinOp \<alpha>\<^isub>1' \<gamma>' \<alpha>\<^isub>2')
      hence "(constFold \<sigma> \<alpha>\<^isub>1) \<guillemotleft>\<gamma>\<guillemotright> (constFold \<sigma> \<alpha>\<^isub>2) = \<alpha>\<^isub>1' \<guillemotleft>\<gamma>'\<guillemotright> \<alpha>\<^isub>2'"
        by (cases "constFold \<sigma> \<alpha>\<^isub>1",
            cases "constFold \<sigma> \<alpha>\<^isub>2",
            simp_all add:option.split[of "\<lambda>x. x = \<alpha>\<^isub>1' \<guillemotleft>\<gamma>'\<guillemotright> \<alpha>\<^isub>2'"])
      hence "(constFold \<sigma> \<alpha>\<^isub>1) = \<alpha>\<^isub>1'" "\<gamma> = \<gamma>'" "constFold \<sigma> \<alpha>\<^isub>2 = \<alpha>\<^isub>2'"
        by simp_all
    moreover
      from assms BinOp calculation have "constFold \<sigma> (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2) = (\<alpha>\<^isub>1' \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2')"
        by simp
    from this calculation(1,3) show thesis ..
next
  case (Var vname)
    thus thesis
      by (cases "constFold \<sigma> \<alpha>\<^isub>1",
          cases "constFold \<sigma> \<alpha>\<^isub>2",
          simp_all add:option.split[of "\<lambda>x. x = Var vname"])
next
  case (Val \<omega>)
    then obtain \<omega>\<^isub>1 \<omega>\<^isub>2
      where "constFold \<sigma> \<alpha>\<^isub>1 = Val \<omega>\<^isub>1"
        and "constFold \<sigma> \<alpha>\<^isub>2 = Val \<omega>\<^isub>2"
        and "binop \<gamma> \<omega>\<^isub>1 \<omega>\<^isub>2 = Some \<omega>"
      by (cases "constFold \<sigma> \<alpha>\<^isub>1",
          cases "constFold \<sigma> \<alpha>\<^isub>2",
          auto simp add:option.split[of "\<lambda>x. x = Val \<omega>"])
    with Val show thesis ..
qed

lemma constFoldBinOpE2:
  fixes \<sigma> \<alpha>\<^isub>1 \<gamma> \<alpha>\<^isub>2
  obtains (Val) \<omega> \<omega>\<^isub>1 \<omega>\<^isub>2
            where "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = Val \<omega>"
                  "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>1) = Val \<omega>\<^isub>1"
                  "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>2) = Val \<omega>\<^isub>2"
                  "binop \<gamma> \<omega>\<^isub>1 \<omega>\<^isub>2 = Some \<omega>"
        | (BinOp) \<alpha>\<^isub>1' \<alpha>\<^isub>2'
            where "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = (\<alpha>\<^isub>1' \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2')"
                  "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>1) = \<alpha>\<^isub>1'"
                  "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>2) = \<alpha>\<^isub>2'"
proof (cases \<alpha>\<^isub>1 \<sigma>\<^isub>2 \<alpha>\<^isub>2 \<gamma> rule:constFoldBinOpE[case_names Val2 BinOp2])
  case (BinOp2 \<alpha>\<^isub>1'' \<alpha>\<^isub>2'')
    show thesis
    proof (cases \<alpha>\<^isub>1'' \<sigma>\<^isub>1 \<alpha>\<^isub>2'' \<gamma> rule:constFoldBinOpE[case_names Val1 BinOp1])
      case (BinOp1 \<alpha>\<^isub>1' \<alpha>\<^isub>2')
      hence "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = \<alpha>\<^isub>1' \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2'"
            "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>1) = \<alpha>\<^isub>1'"
            "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>2) = \<alpha>\<^isub>2'" using BinOp2 by simp_all
      thus thesis ..
    next
      case (Val1 \<omega> \<omega>\<^isub>1 \<omega>\<^isub>2)
      hence "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = Val \<omega>"
            "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>1) = Val \<omega>\<^isub>1"
            "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>2) = Val \<omega>\<^isub>2"
            "binop \<gamma> \<omega>\<^isub>1 \<omega>\<^isub>2 = Some \<omega>" using BinOp2 by simp_all
      thus thesis ..
    qed
next
  case (Val2 \<omega> \<omega>\<^isub>1 \<omega>\<^isub>2)
  hence "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = Val \<omega>"
        "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>1) = Val \<omega>\<^isub>1"
        "constFold \<sigma>\<^isub>1 (constFold \<sigma>\<^isub>2 \<alpha>\<^isub>2) = Val \<omega>\<^isub>2"
        "binop \<gamma> \<omega>\<^isub>1 \<omega>\<^isub>2 = Some \<omega>" by simp_all
  thus thesis ..
qed

lemma constFold_idempt_partial:
  assumes "\<sigma>\<^isub>p \<subseteq>\<^sub>m \<sigma>"
  shows "constFold \<sigma> (constFold \<sigma>\<^isub>p \<alpha>) = constFold \<sigma> \<alpha>"
using assms
proof (induct \<alpha> rule:constFold.induct)
  case (1 \<sigma> \<alpha>\<^isub>1 \<gamma> \<alpha>\<^isub>2)
    show ?case
    proof (cases \<alpha>\<^isub>1 \<sigma> \<sigma>\<^isub>p \<alpha>\<^isub>2 \<gamma> rule:constFoldBinOpE2[case_names Vall BinOpl])
      case (BinOpl \<alpha>\<^isub>1l \<alpha>\<^isub>2l)
      show ?thesis
      proof (cases \<alpha>\<^isub>1 \<sigma> \<alpha>\<^isub>2 \<gamma> rule:constFoldBinOpE[case_names Valr BinOpr])
        case (BinOpr \<alpha>\<^isub>1r \<alpha>\<^isub>2r)
          hence "constFold \<sigma> (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2) = constFold \<sigma> \<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> constFold \<sigma> \<alpha>\<^isub>2"
            by simp
        moreover
          from BinOpl have "constFold \<sigma> (constFold \<sigma>\<^isub>p (\<alpha>\<^isub>1 \<guillemotleft>\<gamma>\<guillemotright> \<alpha>\<^isub>2)) = constFold \<sigma> (constFold \<sigma>\<^isub>p \<alpha>\<^isub>1) \<guillemotleft>\<gamma>\<guillemotright> constFold \<sigma> (constFold \<sigma>\<^isub>p \<alpha>\<^isub>2)"
            by simp
        ultimately show ?thesis using 1 by simp
      next
        case Valr
        thus ?thesis using 1
          by (cases "constFold \<sigma>\<^isub>p \<alpha>\<^isub>1",
              cases "constFold \<sigma>\<^isub>p \<alpha>\<^isub>2",
              simp_all)
      qed
    next
      case Vall
      thus ?thesis using 1 by simp_all
    qed
next
  case (2 \<sigma>)
  fix vname
  from 2 show "constFold \<sigma> (constFold \<sigma>\<^isub>p (Var vname)) = constFold \<sigma> (Var vname)"
    by (cases "vname \<in> dom \<sigma>\<^isub>p", auto simp add:map_le_def split:option.split)
next
  case (3 \<sigma>)
  fix val
  from 3 show "constFold \<sigma> (constFold \<sigma>\<^isub>p (Val val)) = constFold \<sigma> (Val val)"
    by simp
qed

corollary constFold_idempt_fun: "constFold \<sigma> (constFold \<sigma> \<alpha>) = constFold \<sigma> \<alpha>"
by (rule constFold_idempt_partial, simp)

corollary constFold_idempt:
  assumes "constFold \<sigma> \<alpha> = \<alpha>'"
  assumes "constFold \<sigma> \<alpha>' = \<alpha>''"
  shows "\<alpha>' = \<alpha>''"
proof -
    have "\<alpha>' = constFold \<sigma> \<alpha>" using assms by simp
  also
    have "... = constFold \<sigma> (constFold \<sigma> \<alpha>)" by (simp add:constFold_idempt_fun)
  also
    have "... = \<alpha>''" using assms by simp
  finally show ?thesis.
qed

subsubsection {* Propagation *}

theorem constPropagate_idempt:
  assumes "constPropagate \<sigma> \<pi> \<sigma>' \<pi>'"
  assumes "constPropagate \<sigma> \<pi>' \<sigma>'' \<pi>''"
  shows "\<pi>' = \<pi>'' \<and> \<sigma>' = \<sigma>''"
using assms
proof (induction arbitrary:\<sigma>'' \<pi>'' rule:constPropagate.induct)
  case Skip
    thus ?case by (auto elim:constPropagate.cases)
next
  case LAssVal
    thus ?case by (auto elim:constPropagate.cases)
next
  case LAssNonVal
    from LAssNonVal.prems show ?case using LAssNonVal constFold_idempt[OF LAssNonVal.hyps(1)]
      by (cases rule:constPropagate.cases, auto)
next
  case Seq
    thus ?case by (auto elim:constPropagate.cases)
next
  case CondTrue
    thus ?case by simp
next
  case CondFalse
    thus ?case by simp
next
  case Cond
    from Cond.prems show ?case using Cond constFold_idempt[OF Cond.hyps(1)]
      by (cases rule:constPropagate.cases, auto)
next
  case WhileFalse
    thus ?case by (auto elim:constPropagate.cases)
next
  case (While \<sigma> expr)
    hence "constFold \<sigma> (constFold empty expr) \<noteq> Val false"
      by (simp add:constFold_idempt_partial)
    then show ?case using While constFold_idempt_fun
      by (auto elim:constPropagate.cases)
qed

(*>*)
section {* Erhaltung der Typisierbarkeit *}
(*<*)

text {* Wenn Originalemptyanweisung typt, dann auch resultierende Anweisung der
        Konstantenpropagation *}

definition well_typed_state :: "env \<Rightarrow> state \<Rightarrow> bool"
  where "well_typed_state \<Gamma> \<sigma> = (\<forall>v \<in> dom \<sigma>. (\<Gamma> \<turnstile> Val (the (\<sigma> v)) : the (\<Gamma> v)))"

lemma empty_is_well_typed_state: "well_typed_state \<Gamma> empty"
by (simp add:well_typed_state_def)

subsubsection {* Folding *}

lemma binopAndE:
  assumes "binop And lval rval = Some val"
  obtains x where "val = Bool x"
using assms
by (cases "(And,lval,rval)" rule:binop.cases, auto)

lemma binopAddE:
  assumes "binop Add lval rval = Some val"
  obtains x where "val = Intg x"
using assms
by (cases "(Add,lval,rval)" rule:binop.cases, auto)

lemma binopSubE:
  assumes "binop Sub lval rval = Some val"
  obtains x where "val = Intg x"
using assms
by (cases "(Sub,lval,rval)" rule:binop.cases, auto)

lemma binopLessE:
  assumes "binop Less lval rval = Some val"
  obtains x where "val = Bool x"
using assms
by (cases "(Less,lval,rval)" rule:binop.cases, auto)

(*>*)

text_raw {*
\begin{frame}{Typisierbarkeitserhaltung}
  \begin{itemize} *}

text_raw {*
\item
\textup{Gilt nur für Zustände $\sigma$, deren Werte in ihrem Typ mit $\Gamma$ übereinstimmen.}\\
@{text "{ x \<mapsto> Int } \<turnstile> x + 1 : Int"}$\;\not \Longrightarrow\;$ @{text "{ x \<mapsto> Int } \<turnstile> { x \<mapsto> true }\<lparr>x + 1\<rparr> : Int"}\\[1em]
*}

text_raw {* \item *}
lemma constFold_type_preserving:
  "\<lbrakk>\<Gamma> \<turnstile> e : \<tau>; well_typed_state \<Gamma> \<sigma>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<sigma>\<lparr>e\<rparr> : \<tau>"
by (induction rule:expr_typing.induct) (auto(*<*)intro:expr_typing.intros
         split:expr.split option.split
         elim:binopAndE binopLessE binopAddE binopSubE
         simp add:well_typed_state_def dom_def(*>*))

text_raw {* \vspace{0.5em}\item *}
lemma constPropagate_well_typed_state:
  "\<lbrakk>\<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>; well_typed_state \<Gamma> \<sigma>; \<Gamma> \<turnstile> \<pi>\<rbrakk>
  \<Longrightarrow> well_typed_state \<Gamma> \<sigma>'"
proof (induction rule:constPropagate.induct)
txt_raw {* \ldots *}
(*<*)
  case (LAssVal \<sigma> \<alpha> _ v)
      then obtain \<tau> where "\<Gamma> \<turnstile> \<alpha> : \<tau>" "\<Gamma> v = Some \<tau>" by (auto elim:WT.cases)
    moreover
      with LAssVal(2,3) have "\<Gamma> \<turnstile> constFold \<sigma> \<alpha> : \<tau>" by (auto intro:constFold_type_preserving)
    ultimately show ?case using LAssVal by (auto elim:WT.cases simp add:well_typed_state_def)
next
  case (Seq _ lcom \<sigma>\<^isub>i _ rcom)
      hence "\<Gamma> \<turnstile> lcom" by (auto elim:WT.cases)
      with Seq have "well_typed_state \<Gamma> \<sigma>\<^isub>i" by simp
    moreover
      from Seq have "\<Gamma> \<turnstile> rcom" by (auto elim:WT.cases)
    ultimately show ?case using Seq by simp
next
  case (Cond _ _ _ tcom \<sigma>\<^isub>t _ ecom \<sigma>\<^isub>e)
      hence "\<Gamma> \<turnstile> tcom" by (auto elim:WT.cases)
      with Cond have "well_typed_state \<Gamma> \<sigma>\<^isub>t" by simp
    moreover
      with Cond have "\<Gamma> \<turnstile> ecom" by (auto elim:WT.cases)
      with Cond have "well_typed_state \<Gamma> \<sigma>\<^isub>e" by simp
    ultimately show ?case
      by (auto simp add:well_typed_state_def dom_def map_intersect_def split:bool.split)
(*>*)qed(*<*) (auto elim:WT.cases simp add:well_typed_state_def dom_def)

(*>*)
text_raw {* \vspace{0.5em}\item *}
theorem constPropagate_type_preserving:
  "\<lbrakk>\<Gamma> \<turnstile> \<pi>; well_typed_state \<Gamma> \<sigma>; \<langle>\<sigma>,\<pi>\<rangle> \<leadsto> \<langle>\<sigma>',\<pi>'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<pi>'"
proof (induction (*<*)arbitrary:\<sigma> \<pi>' \<sigma>' (*>*)rule:WT.induct)
txt_raw {* \ldots *}
(*<*)
  case WTSkip
    thus ?case by (auto elim:constPropagate.cases intro:WT.intros)
next
  case (WTLAss v \<tau> \<alpha>)
      hence "\<Gamma> \<turnstile> (constFold \<sigma> \<alpha>) : \<tau>" by (auto intro:constFold_type_preserving)
    moreover
      from WTLAss.prems have "\<pi>' = (v::=(constFold \<sigma> \<alpha>))" by (auto elim:constPropagate.cases)
    ultimately show ?case using WTLAss by (auto elim:constPropagate.cases intro:WT.intros)
next
  case (WTSeq \<pi>\<^isub>1 \<pi>\<^isub>2)
    then obtain \<sigma>\<^isub>i \<pi>\<^isub>1' \<pi>\<^isub>2'
      where cp1: "constPropagate \<sigma> \<pi>\<^isub>1 \<sigma>\<^isub>i \<pi>\<^isub>1'"
        and cp2: "constPropagate \<sigma>\<^isub>i \<pi>\<^isub>2 \<sigma>' \<pi>\<^isub>2'"
        and cp:  "\<pi>' = \<pi>\<^isub>1' ;; \<pi>\<^isub>2'"
      by (auto elim:constPropagate.cases)

      from WTSeq cp1 have "\<Gamma> \<turnstile> \<pi>\<^isub>1'" "well_typed_state \<Gamma> \<sigma>\<^isub>i"
        by (auto intro:constPropagate_well_typed_state)
    moreover
      with WTSeq cp2 have "\<Gamma> \<turnstile> \<pi>\<^isub>2'" by simp
    ultimately show ?case using cp by (auto intro:WT.intros)
next
  case (WTCond b \<pi>\<^isub>t \<pi>\<^isub>e)
    from WTCond.prems(2) show ?case using WTCond
    proof (cases rule:constPropagate.cases)
      case (Cond _ _ \<pi>\<^isub>t' _ \<pi>\<^isub>e')
          with WTCond have "\<Gamma> \<turnstile> \<pi>\<^isub>t'" by simp
        moreover
          from Cond WTCond have "\<Gamma> \<turnstile> \<pi>\<^isub>e'" by simp
        moreover
          from WTCond have "\<Gamma> \<turnstile> (constFold \<sigma> b) : Boolean"
            by (auto intro:constFold_type_preserving)
        ultimately show ?thesis using Cond by (auto intro:WT.intros)
    qed simp_all
next
  case (WTWhile b)
    from WTWhile.hyps(1) empty_is_well_typed_state have "\<Gamma> \<turnstile> (constFold empty b) : Boolean"
      by (rule constFold_type_preserving)
    with WTWhile.prems(2) show ?case using WTWhile empty_is_well_typed_state[of \<Gamma>]
      by (cases rule:constPropagate.cases, auto intro:WT.intros)
(*>*)
qed

text_raw {* \end{itemize} \end{frame} *}
(*<*)
end(*>*)
